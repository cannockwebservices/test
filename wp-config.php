<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y6`_^wSBZt*<5U,6^Su`Dm97-C.%w`pra(-HVodW3Sk29N=_ H?S%T^0Y2pN3}kR');
define('SECURE_AUTH_KEY',  'P-W{C96,),xq9G2T#?v3{hD#JrH3~JmQ%hp_L@]NndY)JGX0M.iaCJ1WR-g5P]Rb');
define('LOGGED_IN_KEY',    '`=bA0UtRsaE(4>n*aRuDuz^Vh#I(_MnI6O-_`Z/m-FQ$c^DKP[W>7r629~5rDw7x');
define('NONCE_KEY',        '9NnWL8T5@Xy//$tv<V*tA$vVu9l_F{{^,DwO4815Hb=/W7;+xMj^T5I==oKe^nR]');
define('AUTH_SALT',        '^+zJC&nhOKeax:l+JnWZ^dhZw))>qy$.``MfQtr5fA)%W;]}Pr+84%3Qa>lo|DPX');
define('SECURE_AUTH_SALT', 'L&5=wm|BNGx(hv:}aHZ5U/%aO!t2HX=!Dp)jPb)6290(J+g<t44((7++}-Ww4gjM');
define('LOGGED_IN_SALT',   '=`[o{yvBUc;LXkX3^i&K6]L3cOoH:|[p2H5qHY8u]TYMQ47UzIS5O^h^y8d^0O!g');
define('NONCE_SALT',       '|Z1(VraO#Bh#UqBjXlg,EAG8<#I$>B{A8.]F_pv8}F=]AVF&9p_rLB)~/F::^%+R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
